{% if name == 'coala-bears' %}
{% set test_prevent_skips = false %}
{% endif %}
# Radon requires colorama<0.4, but many other tools listed here will
# install a later version, so when pip tries to install radon it will
# ignore the dependency version difference and only fail when the
# bear is loaded.
colorama<0.4
{% if package_module == 'coalib' %}
argcomplete~=1.8
{%endif%}
{% if language == 'python' %}
{# coverage must be before codecov so pip respects the coverage version #}
# coverage 4.4.2 broke compatibility with coverage-config-reload-plugin
# and thus broke https://github.com/jayvdb/coverage_env_plugin .
# See https://github.com/jayvdb/coverage_config_reload_plugin/issues/1
coverage==4.4.1
coverage-env-plugin~=0.1
coverage-config-reload-plugin~=0.2
codecov~=2.0.5
{% if package_module == 'coalib' %}
freezegun~=0.3.9
{%endif%}
{% endif %}
{# moban dependency ruamel.ordereddict fails to compile
   on alpine python 2.7 #}
moban~=0.5.0 ; python_version > '3.0'
{% if package_module != 'coalib' %}
{# https://gitlab.com/coala/mobans/issues/40 #}
packaging~=16.8
{% endif %}
{% if language == 'python' %}
pytest~={{ pytest_version }}
pytest-cov~=2.4
{% if 'manage.py' is exists %}
pytest-django~=3.3.3
{% endif %}
pytest-env~=0.6.0
{% if test_prevent_skips %}
pytest-error-for-skips~=1.0
{% endif %}
pytest-instafail~=0.3.0
pytest-mock~=1.1
pytest-profiling~=1.3.0
{% if package_module == 'bears' %}
pytest-pythonpath~=0.7.0
{% endif %}
pytest-reorder~=0.1.0
git+https://github.com/jayvdb/pytest-reqs@coala#egg=pytest-reqs
pytest-timeout~=1.3.0
pytest-travis-fold~=1.3.0
requests-mock~=1.2
ipdb~=0.11
{% endif %}
pip<10
six>=1.11.0
{% if language == 'python' %}
{% if 2.7 in python_versions %}
unittest2
{% endif %}
wheel~=0.29
{% endif %}
